package com.shop.dao;

import com.shop.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderDao extends JpaRepository<Order, Long>
{
    List<Order> findAllByCustomerIdEquals(long customerId);
}
