package com.shop.dao;

import com.shop.model.OrderItem;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrderItemDao extends JpaRepository<OrderItem, Long>
{
    List<OrderItem> findOrderItemsByOrderId(Long orderId);
}
