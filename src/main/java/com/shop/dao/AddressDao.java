package com.shop.dao;

import com.shop.model.Address;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

@Repository
public interface AddressDao extends JpaRepository<Address, Long>
{
}
