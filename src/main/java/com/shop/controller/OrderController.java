package com.shop.controller;

import com.shop.dao.OrderDao;
import com.shop.model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/orders")
public class OrderController
{
    private final OrderDao orderDao;

    @Autowired
    public OrderController(OrderDao orderDao)
    {
        this.orderDao = orderDao;
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<Order> getAll() {
        return orderDao.findAll();
    }

    @RequestMapping(value = "{order_id}", method = RequestMethod.GET)
    public Order getAddressById(@PathVariable(value="order_id") final Long id) {
        return orderDao.findOne(id);
    }

    @RequestMapping(value = "/customer/{customer_id}", method = RequestMethod.GET)
    public List<Order> getOrdersByCustomer(@PathVariable(value="customer_id") final Long id) {
        return orderDao.findAllByCustomerIdEquals(id);
    }

}
