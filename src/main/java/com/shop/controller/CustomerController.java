package com.shop.controller;

import com.shop.dao.CustomerDao;
import com.shop.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/customers")
public class CustomerController
{

    private final CustomerDao customerDao;

    @Autowired
    public CustomerController(CustomerDao customerDao)
    {
        this.customerDao = customerDao;
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<Customer> getAll() {
        return customerDao.findAll();
    }

    @RequestMapping(value = "{customer_id}", method = RequestMethod.GET)
    public Customer getCustomerById(@PathVariable(value="customer_id") final Long id) {
        return customerDao.findOne(id);
    }

//    @RequestMapping(value = "{customer_id}", method = RequestMethod.GET)
//    public Set<Address> getCustomerAddress(@PathVariable(value="customer_id") final Long id) {
//        return customerDao.findOne(id).getAddresses();
//    }
}
