package com.shop.controller;

import com.shop.model.Order;
import com.shop.model.OrderItem;
import com.shop.service.AddressService;
import com.shop.service.CustomerService;
import com.shop.service.ItemService;
import com.shop.service.OrderService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
public class MakeOrderController
{
    private static final Logger LOGGER = LogManager.getLogger(MakeOrderController.class);

    private final OrderService orderService;
    private final CustomerService customerService;
    private final ItemService itemService;
    private final AddressService addressService;

    private static final String  CANNOT_PERFORM = "Cannot perform this order.";

    @Autowired
    public MakeOrderController(OrderService orderService, CustomerService customerService, ItemService itemService, AddressService addressService)
    {
        this.orderService = orderService;
        this.customerService = customerService;
        this.itemService = itemService;
        this.addressService = addressService;
    }

    @RequestMapping(value = "/make_order", method = RequestMethod.GET)
    public String order() {
        return "order";
    }

    @ResponseBody
    @RequestMapping(value = "/make_order", method = RequestMethod.POST)
    public String newOrder(@RequestParam Map<String, String> params)
    {
        LOGGER.debug(params);
        long customerId = Long.valueOf(params.get("customer"));
        long addressId =  Long.valueOf(params.get("address"));
        int itemsCount = Integer.valueOf(params.get("items_count"));
        float orderTotalCredits = 0;
        Order order = new Order();
        order.setCustomerId(customerId);
        order.setAddressId(addressId);
        List<OrderItem> itemsList = new ArrayList<>();

        if(!isCustomerExists(customerId))
        {
            return "Customer with id:" + customerId + " not exists. " + CANNOT_PERFORM;
        }
        if(!isAddressExists(addressId))
        {
            return "Address with id:" + addressId + " not exists. " + CANNOT_PERFORM;
        }
        for(;itemsCount > 0; itemsCount--)
        {
            String itemId = params.get("item" + itemsCount);
            Integer number = Integer.valueOf(params.get("number" + itemsCount));
            if(checkItemAvailability(itemId, number))
            {
                orderTotalCredits += addItemToOrder(itemsList, itemId, number);
            }
            else
            {
                return "Ordered items " + itemId + " is not available on stock. " + CANNOT_PERFORM;
            }
        }

        return formAnOrder(order, itemsList, customerId, orderTotalCredits);
    }


    private float addItemToOrder(List<OrderItem> itemsList, String itemId, int itemsNumber)
    {
        float totalCost = -1f;
        float itemCost = itemService.getItemCost(itemId);
        if(itemCost > 0)
        {
            totalCost = itemCost * itemsNumber;
            OrderItem orderItem = new OrderItem();
            orderItem.setItemId(itemId);
            orderItem.setNumberOfItems(itemsNumber);
            itemsList.add(orderItem);
        }
        return totalCost;
    }

    private String formAnOrder(Order order, List<OrderItem> itemsList, long customerId, float orderTotalCredits)
    {
        if(checkCustomerSolvency(customerId, orderTotalCredits) )
        {
            order.setOrderItems(itemsList);
            LOGGER.debug(order);
            orderService.save(order);
            return "THANK YOU! The total order amount is " + orderTotalCredits + " credits.";
        }
        else
        {
            return "Not enough credits to make order!";
        }
    }

    private boolean checkCustomerSolvency(long customerId, float totalAmount)
    {
        float credit = customerService.getCustomerCredit(customerId);
        if(credit >= totalAmount)
        {
            return true;
        }
        LOGGER.debug(customerId + " customer have not enough credits!");
        return false;
    }

    private boolean checkItemAvailability(String itemId, int itemsRequired)
    {
        int stock = itemService.getItemsCount(itemId);
        if(stock >= itemsRequired)
        {
            return true;
        }
        LOGGER.debug(itemId + " items is not enough! Available only " + stock );
        return false;
    }

    private boolean isCustomerExists(long customerId)
    {
        return customerService.isExists(customerId);
    }

    private boolean isAddressExists(long addressId)
    {
        return addressService.isExists(addressId);
    }
}
