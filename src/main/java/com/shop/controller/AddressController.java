package com.shop.controller;

import com.shop.dao.AddressDao;
import com.shop.model.Address;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/addresses")
public class AddressController
{
    @Autowired
    AddressDao addressDao;

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<Address> getAll() {
        return addressDao.findAll();
    }

    @RequestMapping(value = "{address_id}", method = RequestMethod.GET)
    public Address getAddressById(@PathVariable(value="address_id") final Long id) {
        return addressDao.findOne(id);
    }
}
