package com.shop.controller;

import com.shop.dao.ItemDao;
import com.shop.model.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/items")
public class ItemController
{
    private final ItemDao itemDao;

    @Autowired
    public ItemController(ItemDao itemDao)
    {
        this.itemDao = itemDao;
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<Item> getAll() {
        return itemDao.findAll();
    }

    @RequestMapping(value = "{item_id}", method = RequestMethod.GET)
    public Item getAddressById(@PathVariable(value="item_id") final String id) {
        return itemDao.findOne(id);
    }
}