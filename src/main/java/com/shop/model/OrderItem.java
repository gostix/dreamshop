package com.shop.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "order_item")
public class OrderItem
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "order_id")
    private Long orderId;

    @Column(name = "item_id")
    private String itemId;

    @Column(name = "number_of_items")
    private Integer numberOfItems;

    @ManyToOne
    @JoinTable(name = "orders", joinColumns = @JoinColumn(name = "id"))
    private Order order;
}
