package com.shop.model;

import lombok.Data;
import javax.persistence.Id;

import javax.persistence.*;

@Data
@Entity
@Table(name = "customers")
public class Customer
{
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "email")
    private String email;

    @Column(name = "credit")
    private float credit;

//    @OneToMany(targetEntity=Order.class, mappedBy="customer", fetch=FetchType.EAGER)
//    private Set<Order> orders;

//    @OneToMany(targetEntity=Address.class, mappedBy="customer", fetch=FetchType.EAGER)
//    private Set<Address> addresses;

}
