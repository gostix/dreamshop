package com.shop.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "addresses")
public class Address
{
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    @Column(name = "street")
    private String street;

    @Column(name = "house")
    private String house;

    @Column(name = "flat")
    private int flat;

//    @ManyToOne
//    @JoinTable(name = "customer", joinColumns = @JoinColumn(name = "id"))
//    private Customer customer;
}
