package com.shop.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "items")
public class Item
{
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private String id;

    @Column(name = "name")
    private String name;

    @Column(name = "cost")
    private float cost;

    @Column(name = "stock")
    private  int stock;
}
