package com.shop.service;

public interface AddressService
{
    boolean isExists(long addressId);
}
