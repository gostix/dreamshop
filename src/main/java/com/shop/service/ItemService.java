package com.shop.service;

public interface ItemService
{
    int getItemsCount(String itemId);
    float getItemCost(String itemId);
    boolean isExists(String itemId);
}
