package com.shop.service;

import com.shop.dao.CustomerDao;
import com.shop.model.Customer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerServiceImpl implements CustomerService
{
    private static final Logger LOGGER = LogManager.getLogger(CustomerServiceImpl.class);

    private final CustomerDao customerDao;

    @Autowired
    public CustomerServiceImpl(CustomerDao customerDao)
    {
        this.customerDao = customerDao;
    }

    @Override
    public float getCustomerCredit(long customerId)
    {
        float credit = -1;
        Customer customer = findCustomerById(customerId);
        if(customer != null)
        {
            credit = customer.getCredit();
        }
        return credit;
    }

    @Override
    public boolean isExists(long customerId)
    {
        return customerDao.exists(customerId);
    }

    @Override
    public Customer findCustomerById(long customerId)
    {
        Customer customer = null;
        if(isExists(customerId))
        {
            customer = customerDao.findOne(customerId);
        }
        else
        {
            LOGGER.debug("Customer with id:" + customerId + " not exists.");
        }
        return customer;
    }

}
