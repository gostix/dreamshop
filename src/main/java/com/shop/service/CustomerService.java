package com.shop.service;

import com.shop.model.Customer;

public interface CustomerService
{
    float getCustomerCredit(long customerId);
    boolean isExists(long customerId);
    Customer findCustomerById(long customerId);
}
