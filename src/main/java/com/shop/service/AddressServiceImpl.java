package com.shop.service;

import com.shop.dao.AddressDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AddressServiceImpl implements AddressService
{
    private final AddressDao addressDao;

    @Autowired
    public AddressServiceImpl(AddressDao addressDao)
    {
        this.addressDao = addressDao;
    }

    @Override
    public boolean isExists(long addressId)
    {
        return addressDao.exists(addressId);
    }
}
