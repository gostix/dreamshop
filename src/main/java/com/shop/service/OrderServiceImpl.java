package com.shop.service;

import com.shop.dao.AddressDao;
import com.shop.dao.CustomerDao;
import com.shop.dao.OrderDao;
import com.shop.dao.OrderItemDao;
import com.shop.model.Address;
import com.shop.model.Customer;
import com.shop.model.Order;
import com.shop.model.OrderItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderServiceImpl implements OrderService
{
    private final CustomerDao customerDao;

    private final OrderDao orderDao;

    private final OrderItemDao orderItemDao;

    private final AddressDao addressDao;

    @Autowired
    public OrderServiceImpl(CustomerDao customerDao, OrderDao orderDao, OrderItemDao orderItemDao, AddressDao addressDao)
    {
        this.customerDao = customerDao;
        this.orderDao = orderDao;
        this.orderItemDao = orderItemDao;
        this.addressDao = addressDao;
    }

    @Override
    public void save(Order order)
    {
        Order o = orderDao.saveAndFlush(order);
        for (OrderItem item : order.getOrderItems())
        {
            item.setOrderId(o.getId());
            orderItemDao.save(item);
        }
    }

    @Override
    public List<Order> getOrdersByCostumer(long costumerId)
    {
        return orderDao.findAllByCustomerIdEquals(costumerId);
    }

    @Override
    public List<OrderItem> getOrderedItemsByOrder(long orderId)
    {
        return orderItemDao.findOrderItemsByOrderId(orderId);
    }

    @Override
    public Address getAddressByOrder(long orderId)
    {
        Long addressId = orderDao.findOne(orderId).getAddressId();
        return addressDao.getOne(addressId);
    }

    @Override
    public Customer getCustomerByOrder(long orderId)
    {
        Long customerId = orderDao.findOne(orderId).getCustomerId();
        return customerDao.findOne(customerId);
    }
}
