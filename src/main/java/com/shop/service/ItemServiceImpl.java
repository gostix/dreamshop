package com.shop.service;

import com.shop.dao.ItemDao;
import com.shop.model.Item;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class ItemServiceImpl implements ItemService
{
    private static final Logger LOGGER = LogManager.getLogger(ItemServiceImpl.class);
    private final ItemDao itemDao;

    @Autowired
    public ItemServiceImpl(ItemDao itemDao)
    {
        this.itemDao = itemDao;
    }

    @Override
    public int getItemsCount(String itemId)
    {
        int itemsCount = -1;
        Item item = findItem(itemId);
        if(item != null)
        {
            itemsCount = item.getStock();
        }
        return itemsCount;
    }

    @Override
    public float getItemCost(String itemId)
    {
        float itemCost = -1;
        Item item = findItem(itemId);
        if(item != null)
        {
            itemCost = item.getCost();
        }
        return itemCost;
    }

    @Override
    public boolean isExists(String itemId)
    {
        return itemDao.exists(itemId);
    }

    private Item findItem(String itemId)
    {
        Item item = null;
        if(isExists(itemId))
        {
            item =  itemDao.getOne(itemId);
        }
        else
        {
            LOGGER.error("Item with id:" + itemId + " not exists.");
        }
        return item;
    }


}
