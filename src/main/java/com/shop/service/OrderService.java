package com.shop.service;

import com.shop.model.Address;
import com.shop.model.Customer;
import com.shop.model.Order;
import com.shop.model.OrderItem;

import java.util.List;

public interface OrderService
{
    void save(Order order);

    List<Order> getOrdersByCostumer(long costumerId);

    List<OrderItem> getOrderedItemsByOrder(long orderId);

    Address getAddressByOrder(long orderId);

    Customer getCustomerByOrder(long orderId);
}
