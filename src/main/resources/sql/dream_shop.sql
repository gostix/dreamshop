create sequence orders_id_seq
;

create sequence order_item_id_seq
;

create sequence addresses_id_seq
;

create sequence customers_id_seq
;

create table items
(
  name varchar(100) default NULL::character varying,
  cost numeric(10,2) default 0.00 not null,
  stock integer default 0 not null,
  id varchar(12) not null
    constraint item_pk
    primary key
)
;

create unique index item_uindex
  on items (id)
;

create table addresses
(
  street varchar(100) default NULL::character varying,
  house varchar(32) default NULL::character varying,
  flat integer,
  id serial not null
    constraint addresses_id_pk
    primary key
)
;

create unique index addresses_id_uindex
  on addresses (id)
;

create table order_item
(
  order_id integer,
  item_id varchar(12) not null,
  number_of_items integer not null,
  id serial not null
    constraint order_item_id_pk
    primary key
)
;

create unique index order_item_id_uindex
  on order_item (id)
;

create table customers
(
  name varchar(100) default NULL::character varying,
  email varchar(100) default NULL::character varying,
  credit numeric(10,2) default 0.00,
  id serial not null
    constraint customers_id_pk
    primary key
)
;

create unique index customers_id_uindex
  on customers (id)
;

create table orders
(
  customer_id integer,
  address_id integer,
  id serial not null
    constraint orders_id_pk
    primary key
)
;

create unique index orders_id_uindex
  on orders (id)
;

